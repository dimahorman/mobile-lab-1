# Mobile Lab 1. React Native
## Run 

### Prerequisites
1. [Install Expo](https://docs.expo.io/get-started/installation/)


### Run Locally
1. Clone repository: `git clone https://gitlab.com/dimahorman/mobile-lab-1.git`
2. Go to cloned project `cd mobile-lab-1`
3. Run `npm install`
4. Run `expo start`

### View application
Go to `http://localhost:19006/` in your browser
