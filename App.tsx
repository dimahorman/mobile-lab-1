import React from 'react';
import {SignInComponent} from "./components/SignInComponent";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from "@react-navigation/stack";
import {SignUpComponent} from "./components/SignUpComponent";
import {HomeComponent} from "./components/HomeComponent";

export default class App extends React.Component {
    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name="Sign in" component={SignInComponent}/>
                    <Stack.Screen name="Home" component={HomeComponent}/>
                    <Stack.Screen name="Sign Up" component={SignUpComponent}/>
                </Stack.Navigator>
            </NavigationContainer>
        );
    }
}

const Stack = createStackNavigator();
