import * as firebase from 'firebase';
import 'firebase/firestore';

/*Provide your firebase secrets */
const firebaseConfig = {
    apiKey: "apiKey",
    authDomain: "authDomain",
    databaseURL: "databaseURL",
    projectId: "projectId",
    storageBucket: "storageBucket",
    messagingSenderId: "messagingSenderId",
    appId: "appId"
};
firebase.initializeApp(firebaseConfig);

export default firebase;
