export const themes = {
    light: {
        textColor: "black",
        backgroundColor: "white"
    },
    dark: {
        textColor: "white",
        backgroundColor: "#272729"
    }
};
